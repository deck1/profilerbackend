from django.urls import path
from .views import *

urlpatterns = [
    path(r'test',test),
    path(r'mention_highchart',mention_highchart,name='mention_highchart'),
    path(r'mention_network',mention_network,name='mention_highchart'),
    path(r'filter_mention_highchart',filter_mention_highchart,name='filter_mention_highchart'),
    path(r'filter_mention_network',filter_mention_network),
    path(r'networks',NetworkApi.as_view(),name='filter_mention_highchart'),
    path(r'graphstats',GraphStatApi.as_view()),
    path(r'weighted_network',weighted_network),
    path(r'filter_weighted_network',filter_weighted_network),
    path(r'top_n_handlers',top_n_handlers),
    path(r'filter_top_n_handlers',filter_top_n_handlers),
    path(r'timeline',timeline),
    path(r'filter_timeline',filter_timeline),
    path(r'cumtimeline',cumtimeline),
    path(r'processshedulers',ProcesssSchedulerApi.as_view(),name='loanapplication-create'),
    path(r'filter_cumtimeline',filter_cumtimeline),
    path(r'processshedulers/<int:id>', ProcesssSchedulerDetailApi.as_view()),   
]