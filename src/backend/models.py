from django.db import models
import time
import random
from djongo import models as modeldb

# Create your models here.

class ProcessSheduler(models.Model):
    class Meta:
        db_table = "tbl_scheduler"
    id = models.CharField(max_length=200,primary_key=True)
    jobname = models.CharField(max_length=200)
    process = models.CharField(max_length=200)
    isweekly = models.CharField(max_length=200)
    ismonthly = models.CharField(max_length=200)
    isdaily = models.CharField(max_length=200)
    ishourly = models.CharField(max_length=200)
    month = models.CharField(max_length=200)
    monthday = models.CharField(max_length=200)         
    weekday = models.CharField(max_length=200)
    hour = models.CharField(max_length=200)
    minute = models.CharField(max_length=200)
    second = models.CharField(max_length=200)
    dayofweek = models.CharField(max_length=200)
    status=models.CharField(max_length=200)

class GraphStat(models.Model):
    class Meta:
        db_table="tbl_graph_stat"
        pass
    totalcnt=models.FloatField()
    mean=models.FloatField()
    std=models.FloatField()
    min=models.FloatField()
    firstquantile=models.FloatField()
    secondquantile=models.FloatField()
    thirdquantile=models.FloatField()
    max=models.FloatField()
    networkid=models.CharField(max_length=300)
    networkname=models.CharField(max_length=300)
    
class Edge(models.Model):
    class Meta:
        db_table="edges"
    
    edge=models.CharField(max_length=300)
    weight=models.FloatField()
    networkname=models.CharField(max_length=300)
    networkid=models.CharField(max_length=300)

class Network(models.Model):
    class Meta:
        db_table="network_detail"
    
    networkname=models.CharField(max_length=300)
    networkid=models.CharField(max_length=300)

class EdgeDetail(models.Model):
    class Meta:
        db_table="tbl_edge_detail"
    
    edge=models.CharField(max_length=300)
    weight=models.FloatField()
    node_a=models.CharField(max_length=300)
    node_b=models.CharField(max_length=300)
    networkid=models.CharField(max_length=300)
    networkname=models.CharField(max_length=300)


class WeighedNodes(models.Model):
    class Meta:
        db_table="tbl_weighted_nodes"
        pass
    mention=models.CharField(max_length=300)
    weighed=models.FloatField()
    username=models.CharField(max_length=300)
    networkid=models.CharField(max_length=300)
    networkname=models.CharField(max_length=300)



class EdgeNumber(models.Model):
    class Meta:
        db_table="tbl_edge_number"
        pass
    username=models.CharField(max_length=300)
    edge=models.FloatField()
    networkid=models.CharField(max_length=300)
    networkname=models.CharField(max_length=300)


class AllData(models.Model):
    class Meta:
        db_table="tbl_tweets"
        pass
    description=models.CharField(max_length=300)
    followers=models.CharField(max_length=300)
    location=models.CharField(max_length=300)
    name=models.CharField(max_length=300)
    networkid=models.CharField(max_length=300)
    networkname=models.CharField(max_length=300)
    numberstatuses=models.CharField(max_length=300)
    time=models.CharField(max_length=300)
    tweets=models.CharField(max_length=300)
    username=models.CharField(max_length=300)