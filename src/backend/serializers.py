from rest_framework import serializers 
from .models import *
import os
import uuid

class EdgeSerializer(serializers.ModelSerializer):
    class Meta:
        model=Edge
        fields="__all__"

class NetworkSerializer(serializers.ModelSerializer):
    class Meta:
        model=Network
        fields="__all__"

class GraphStatSerializer(serializers.ModelSerializer):
    class Meta:
        model=GraphStat
        fields="__all__"


class ProcessShedulerSerializer(serializers.ModelSerializer):
    class Meta:
        model=ProcessSheduler
        fields="__all__"
