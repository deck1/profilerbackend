from django.shortcuts import render
from rest_framework import generics, mixins
from rest_framework import filters
import requests
import uuid
import time
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework.decorators import api_view
from .models import *
from .serializers import *
from django.db.models import Count
from django.db.models import Func, Sum
import numpy as np
import pandas as pd
# Create your views here.


@api_view(['POST','GET'])
def filter_cumtimeline(request):
    df=None
    if(request.data['network']=='N/A'):
        df=pd.DataFrame(list(AllData.objects.filter(time__gte=request.data['minvalue'],time__lte=request.data['maxvalue']).order_by('time').values()))
        pass
    else:
        df=pd.DataFrame(list(AllData.objects.filter(time__gte=request.data['minvalue'],time__lte=request.data['maxvalue'],networkname=request.data['network']).order_by('time').values()))
        pass
    return JsonResponse({'names': list(df.time.value_counts().sort_index().cumsum().index),'number':list(df.time.value_counts().sort_index().cumsum())}, status=200)


@api_view(['POST','GET'])
def filter_timeline(request):
    names=[]
    number=[]
    if(request.data['network']=='N/A'):
        datas=AllData.objects.values('time').filter(time__gte=request.data['minvalue'],time__lte=request.data['maxvalue']).annotate(total=Count('time')).order_by('time')
        for d in list(datas):
            names.append(d['time'])
            number.append(d['total'])
            pass
        pass
    else:
        datas=AllData.objects.values('time').filter(time__gte=request.data['minvalue'],time__lte=request.data['maxvalue'],networkname=request.data['network']).annotate(total=Count('time')).order_by('time')
        for d in list(datas):
            names.append(d['time'])
            number.append(d['total'])
            pass
        pass
   
    return JsonResponse({'names': names,'number':number}, status=200)

@api_view(['GET'])
def test(request):
    return JsonResponse({'message': 'test'}, status=200)

@api_view(['GET'])
def cumtimeline(request):
    df=pd.DataFrame(list(AllData.objects.all().order_by('time').values()))
    return JsonResponse({'names': list(df.time.value_counts().sort_index().cumsum().index),'number':list(df.time.value_counts().sort_index().cumsum())}, status=200)


@api_view(['GET'])
def timeline(request):
    datas=AllData.objects.values('time').annotate(total=Count('time')).order_by('time')
    names=[]
    number=[]
    for d in list(datas):
        names.append(d['time'])
        number.append(d['total'])
        pass
    return JsonResponse({'names': names,'number':number}, status=200)


@api_view(['GET'])
def top_n_handlers(request):
    datas=AllData.objects.values('username').annotate(total=Count('username')).order_by('-total')[0:10]
    names=[]
    number=[]
    for d in list(datas):
        names.append(d['username'])
        number.append(d['total'])
        pass
    return JsonResponse({'names': names,'number':number}, status=200)

@api_view(['POST','GET'])
def filter_top_n_handlers(request):
    datas=None
    names=[]
    number=[]   
    if(request.data['network']=='N/A'):
        datas=AllData.objects.values('username').annotate(total=Count('username')).order_by('-total')[0:int(request.data['topn'])]
        for d in list(datas):
            names.append(d['username'])
            number.append(d['total'])
            pass
        pass
    else:
        datas=AllData.objects.values('username').filter(networkname=request.data['network']).annotate(total=Count('username')).order_by('-total')[0:int(request.data['topn'])]
        for d in list(datas):
            names.append(d['username'])
            number.append(d['total'])
            pass
        pass
    return JsonResponse({'names': names,'number':number}, status=200)

@api_view(['GET'])
def weighted_network(request):
    array=[]
    datas=WeighedNodes.objects.filter(weighed__gte=10)
    for data in datas:
        datatwo=EdgeNumber.objects.filter(networkid=data.networkid,username=data.username)
        for dtwo in datatwo:
            array.append([data.username,data.mention,int(dtwo.edge)])
            pass
        
        pass 
    return JsonResponse({'message': array}, status=200)

@api_view(['GET'])
def mention_network(request):
    datas=EdgeDetail.objects.all()[:400]
    array=[]
    for data in datas:
        array.append([data.node_a,data.node_b])
        pass 
    return JsonResponse({'message': array}, status=200)

@api_view(['GET'])
def mention_highchart(request):
    datas=Edge.objects.all()
    array=[]
    for data in datas:
        array.append(int(data.weight))
        pass 
    return JsonResponse({'message': array}, status=200)

@api_view(['POST','GET'])
def filter_mention_highchart(request):
    array=[]
    datas=None
    if(request.data['network']=='N/A'):
        datas=Edge.objects.filter(weight__gte=request.data['minvalue'],weight__lte=request.data['maxvalue'])
        pass
    else:
        datas=Edge.objects.filter(weight__gte=request.data['minvalue'],weight__lte=request.data['maxvalue'],networkname=request.data['network'])
    for data in datas:
        array.append(int(data.weight))
        pass 

    return JsonResponse({'message': array}, status=200)

@api_view(['POST','GET'])
def filter_mention_network(request):
    array=[]
    datas=None
    if(request.data['network']=='N/A'):
        datas=EdgeDetail.objects.filter(weight__gte=request.data['minvalue'],weight__lte=request.data['maxvalue'])[0:400]
        pass
    else:
        datas=EdgeDetail.objects.filter(weight__gte=request.data['minvalue'],weight__lte=request.data['maxvalue'],networkname=request.data['network'])[0:400]
    for data in datas:
        array.append([data.node_a,data.node_b])
        pass
    return JsonResponse({'message': array}, status=200)



@api_view(['GET','POST'])
def filter_weighted_network(request):
    array=[]
    datas=None
    if(request.data['network']=='N/A'):
        datas=WeighedNodes.objects.filter(weighed__gte=request.data['minvalue'],weighed__lte=request.data['maxvalue'])
        for data in datas:
            datatwo=EdgeNumber.objects.filter(networkid=data.networkid,username=data.username)
            for dtwo in datatwo:
                array.append([data.username,data.mention,int(dtwo.edge)])
                pass
        pass
    else:
        datas=WeighedNodes.objects.filter(weighed__gte=request.data['minvalue'],weighed__lte=request.data['maxvalue'],networkname=request.data['network'])
        for data in datas:
            datatwo=EdgeNumber.objects.filter(networkid=data.networkid,username=data.username)
            for dtwo in datatwo:
                array.append([data.username,data.mention,int(dtwo.edge)])
                pass

        pass
  
    return JsonResponse({'message': array}, status=200)


class NetworkApi(generics.ListAPIView):
    resource_name = 'networks'
    serializer_class = NetworkSerializer

    def get_queryset(self):
        return Network.objects.all()

class GraphStatApi(generics.ListAPIView):
    resource_name = 'graphstats'
    serializer_class = GraphStatSerializer

    def get_queryset(self):
        return GraphStat.objects.all()


class ProcesssSchedulerApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'processshedulers'
    serializer_class = ProcessShedulerSerializer

    def get_queryset(self):
        return ProcessSheduler.objects.all()


class ProcesssSchedulerDetailApi(generics.RetrieveUpdateDestroyAPIView):
    resource_name = 'processshedulers'
    lookup_field = 'id'
    serializer_class = ProcessShedulerSerializer

    def get_queryset(self):
        return ProcessSheduler.objects.all()